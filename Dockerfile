##############################################
# Build virtualenv
##############################################
FROM python:3.10.5-bullseye AS venv

# Prepare system
##############################################
# https://python-poetry.org/docs/#installation
ENV POETRY_VERSION=1.1.13
RUN curl -sSL https://install.python-poetry.org | python3 -

ENV PATH /root/.local/bin:$PATH

WORKDIR /app

# Install python dependencies
##############################################
COPY pyproject.toml poetry.lock ./

RUN python -m venv --copies /app/venv \
    && . /app/venv/bin/activate \
    && poetry install --no-dev

# Collect static files & build assets
##############################################
ENV PATH /app/venv/bin:$PATH
WORKDIR /app/src
COPY ./src ./

# Build assets so that we don't need the build tools later
RUN python manage.py collectstatic --noinput --clear



##############################################
# write git info
##############################################

## Get git versions
FROM alpine/git:v2.30.2 AS git
ADD . /app
WORKDIR /app
RUN git rev-parse HEAD | tee /version



##############################################
# Main image
##############################################
FROM python:3.10.5-slim-bullseye as prod

ENV TZ "Europe/Paris"

COPY --from=venv /app/venv /app/venv/
ENV PATH /app/venv/bin:$PATH
ENV PYTHONPATH $PYTHONPATH:/app

WORKDIR /app
COPY pyproject.toml ./
COPY contrib/run ./run
COPY src ./src/
COPY --from=git /version /app/.version
ARG STATIC_ROOT=/app/src/staticfiles
COPY --chown=django:django --from=venv $STATIC_ROOT $STATIC_ROOT

HEALTHCHECK --start-period=30s CMD python -c "import requests; requests.get('http://localhost:8000', timeout=2)"

WORKDIR /app/src
CMD ["/app/run"]
