from django.contrib import admin
from django.contrib.auth import get_permission_codename
from django.contrib.auth.admin import UserAdmin
from django.http import HttpRequest
from django.utils.safestring import mark_safe

from redirect.models import Redirect

from .models import RedirectUser


@admin.register(Redirect)
class RedirectAdmin(admin.ModelAdmin):
    list_display = ["short_code", "link", "owner", "group_owner", "target_url"]

    def changelist_view(self, request, extra_context=None):
        self.request = request
        return super().changelist_view(request, extra_context)

    def has_change_permission(self, request: HttpRequest, obj: Redirect = None):
        opts = self.opts
        codename = get_permission_codename("change", opts)
        return request.user.has_perm(f"{opts.app_label}.{codename}", obj)

    def has_delete_permission(self, request: HttpRequest, obj: Redirect = None):
        opts = self.opts
        codename = get_permission_codename("delete", opts)
        return request.user.has_perm(f"{opts.app_label}.{codename}", obj)

    def has_view_permission(self, request: HttpRequest, obj: Redirect = None):
        opts = self.opts
        codename = get_permission_codename("view", opts)
        return request.user.has_perm(
            f"{opts.app_label}.{codename}", obj
        ) or self.has_change_permission(request, obj)

    def get_changeform_initial_data(self, request):
        get_data = super().get_changeform_initial_data(request)
        get_data["owner"] = request.user.pk
        return get_data

    def link(self, instance: Redirect) -> str:
        url = instance.get_absolute_url()
        url = self.request.build_absolute_uri(url)
        return mark_safe(f'<a href="{url}">link</a>')  # noqa: S308, S703


admin.site.register(RedirectUser, UserAdmin)
