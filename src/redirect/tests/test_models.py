from django.contrib.auth.models import AbstractUser, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db import IntegrityError
from django.test import TestCase
from model_bakery import baker

from redirect.models import Redirect, RedirectUser


class RedirectModelTestCase(TestCase):
    def test_has_short_code(self):
        baker.make("Redirect", short_code="potain3")

    def test_short_code_is_unique(self):
        baker.make("Redirect", short_code="potain3")
        with self.assertRaises(IntegrityError):
            baker.make("Redirect", short_code="potain3")

    def test_has_target_url(self):
        baker.make("Redirect", target_url="https://static.augendre.info/potain3")

    def test_can_miss_owner(self):
        baker.make("Redirect", owner=None)

    def test_can_have_owner(self):
        user = RedirectUser.objects.create_user("user")
        baker.make("Redirect", owner=user)

    def test_can_miss_group_owner(self):
        baker.make("Redirect", group_owner=None)

    def test_can_have_group_owner(self):
        group = Group.objects.create(name="group")
        baker.make("Redirect", group_owner=group)


class RedirectModelPermissionsTestMixin:
    def test_owner_can_change_object(self):
        assert self.owner.has_perm("redirect.change_redirect", self.redirect)

    def test_owner_can_delete_object(self):
        assert self.owner.has_perm("redirect.delete_redirect", self.redirect)

    def test_owner_can_view_object(self):
        assert self.owner.has_perm("redirect.view_redirect", self.redirect)

    def test_non_owner_cant_change_object(self):
        assert not self.non_owner.has_perm("redirect.change_redirect", self.redirect)

    def test_non_owner_cant_delete_object(self):
        assert not self.non_owner.has_perm("redirect.delete_redirect", self.redirect)


class RedirectModelOwnerPermissionsTestCase(
    RedirectModelPermissionsTestMixin, TestCase
):
    @classmethod
    def setUpTestData(cls):
        cls.owner: AbstractUser = RedirectUser.objects.create_user("owner")
        cls.non_owner: AbstractUser = RedirectUser.objects.create_user("rando")
        content_type = ContentType.objects.get_for_model(Redirect)
        permission = Permission.objects.get(
            codename="view_redirect",
            content_type=content_type,
        )
        cls.non_owner.user_permissions.add(permission)
        cls.redirect = baker.make("Redirect", owner=cls.owner)


class RedirectModelGroupOwnerPermissionsTestCase(
    RedirectModelPermissionsTestMixin, TestCase
):
    @classmethod
    def setUpTestData(cls):
        cls.owner: AbstractUser = RedirectUser.objects.create_user("owner")
        cls.non_owner: AbstractUser = RedirectUser.objects.create_user("rando")
        group_owner = Group.objects.create(name="group_owner")
        group_owner.user_set.add(cls.owner)
        cls.redirect = baker.make("Redirect", group_owner=group_owner)
