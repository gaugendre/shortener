from django.test import TestCase
from django.urls import reverse

from redirect.models import Redirect


class RedirectViewTestCase(TestCase):
    def test_redirect_to_target_url(self):
        short_code = "potain3"
        target_url = "https://static.augendre.info/potain3/"
        Redirect.objects.create(short_code=short_code, target_url=target_url)
        url = reverse("redirect", kwargs={"short_code": short_code})
        res = self.client.get(url)
        assert res.status_code == 302
        assert res.url == target_url

    def test_non_existent_short_code(self):
        short_code = "potain3"
        target_url = "https://static.augendre.info/potain3/"
        Redirect.objects.create(short_code=short_code, target_url=target_url)
        url = reverse("redirect", kwargs={"short_code": short_code + "4"})
        res = self.client.get(url)
        assert res.status_code == 404


class HomeViewTestCase(TestCase):
    def test_home_redirects_to_admin(self):
        url = reverse("home")
        res = self.client.get(url)
        assert res.status_code == 302
        assert res.url == reverse("admin:index")
