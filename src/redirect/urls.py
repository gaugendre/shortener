from django.urls import path

from redirect.views import home, redirect_view

urlpatterns = [
    path("", home, name="home"),
    path("<str:short_code>", redirect_view, name="redirect"),
]
