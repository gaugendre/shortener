from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import AbstractUser

from redirect.models import Redirect


class RedirectBackend(ModelBackend):
    def has_perm(self, user_obj: AbstractUser, perm, obj=None):
        allowed = super().has_perm(user_obj, perm)
        if allowed:
            return allowed
        if obj and isinstance(obj, Redirect):
            return obj.owner == user_obj or obj.group_owner in user_obj.groups.all()
        return False
