from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from django.urls import reverse


class Redirect(models.Model):
    short_code = models.CharField(max_length=250, blank=False, null=False, unique=True)
    target_url = models.URLField(max_length=50_000)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        related_name="owned_redirects",
        null=True,
        blank=True,
        help_text="Give any user full permissions over this object. "
        "The user will still at least need global view permissions "
        "for the admin to work.",
    )
    group_owner = models.ForeignKey(
        Group,
        on_delete=models.SET_NULL,
        related_name="owned_redirects",
        null=True,
        blank=True,
        help_text="Give any group full permissions over this object."
        "The user will still at least need global view permissions "
        "for the admin to work.",
    )

    def __str__(self):
        return f"{self.short_code} => {self.target_url}"

    def get_absolute_url(self):
        return reverse("redirect", args=(self.short_code,))


class RedirectUser(AbstractUser):
    pass
