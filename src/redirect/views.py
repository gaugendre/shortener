from django.shortcuts import get_object_or_404
from django.shortcuts import redirect as django_redirect

from redirect.models import Redirect


def redirect_view(request, short_code):
    redirect = get_object_or_404(Redirect, short_code=short_code)
    return django_redirect(redirect.target_url)


def home(request):
    return django_redirect("admin:index")
